package classes;

public class Pizza  extends Dish {
    private String extras;

    public Pizza() {
        super( 30);

    }


    public Pizza( String extras) {
        super( 45);
        this.extras = extras;
    }



    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }
}
