package classes;

import java.util.Scanner;

public class Menu {
    public static void runMenu(Restaurant restaurant) {
        // Scanner for user input
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\nMenu:");
            System.out.println("1. Add an order");
            System.out.println("2. Print all orders");
            System.out.println("3. Exit");
            System.out.print("Please choose an option (1/2/3): ");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    // Create a Dish


                    // Create an Order
                    System.out.print("Enter your name : ");
                    String name = scanner.next();

                    System.out.print("Enter City: ");
                    String city = scanner.next();
                    System.out.print("Enter a dish type (PASTA, PIZZA): ");
                    String input = scanner.next();

                    // Convert the user input to an Enum value
                    DishType dishType = null;
                    try {
                        dishType = DishType.valueOf(input.toUpperCase());
                    } catch (IllegalArgumentException e) {
                        System.out.println("Invalid dish type.");

                        continue;
                    }
                    Dish dish= DishFactory.createDish(dishType);

                    if(dish==null){
                        continue;
                    }
                    Order order = new Order(name, city, dish);

                    // Add the Order to the Restaurant
                    restaurant.addOrder(order);
                    System.out.println("Order added successfully.");
                    break;

                case 2:
                    // Print all orders in the Restaurant
                    restaurant.printAllOrders();
                    continue;

                case 3:
                    // Exit the menu
                    System.out.println("Exiting the menu.");

                    continue;

                default:
                    System.out.println("Invalid choice. Please choose a valid option (1/2/3).");

            }
        }
    }
}
