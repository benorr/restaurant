package classes;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class OrderChecker extends Thread {
    private Order[] orders = new Order[3];

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.MINUTES.sleep(1);
                Restaurant.lock.lock();
                try {
                    // Check if the queue is not empty
                    if (!Restaurant.ordersQueue.isEmpty()) {
                        // Process the first 3 orders
                        int processedOrders = 0;
                        while (processedOrders < 3 && !Restaurant.ordersQueue.isEmpty()) {
                            Order order = Restaurant.ordersQueue.poll();
                            if (order != null) {
                                String city = order.getCity();
                                List<Order> orderList = Restaurant.hashMap.get(city);
                                System.out.println();
                                System.out.println("Processing Order: ");
                                orders[processedOrders++] = order;
                                if (orderList.isEmpty()) {
                                    continue;
                                } else {
                                    for (Order _order : orderList) {
                                        if (processedOrders < 3) {
                                            orders[processedOrders] = _order;
                                            Restaurant.deleteFromOrderList(city, _order);
                                            processedOrders++;
                                        }
                                    }
                                }
                            }
                        }
                        System.out.println();
                        System.out.println("Delivery go out with: ");

                        for (int i = 0; i < processedOrders; i++) {
                            if(orders[i]!=null){

                            orders[i].displayOrder();
                            }

                        }

                    } else {
                        System.out.println("No orders in the queue.");
                    }
                } finally {
                    Restaurant.lock.unlock(); // Release the lock in a finally block to ensure it's always released
                }

            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

}
