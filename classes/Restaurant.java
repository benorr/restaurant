package classes;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Restaurant {
    // Static queue to hold orders

    public static Queue<Order> ordersQueue = new LinkedList<>();
    public static Map<String, List<Order>> hashMap = new HashMap<>();
     static Lock lock = new ReentrantLock();
    // Add an order to the queue
    public static void addOrder(Order order) {

        String city = order.getCity();

        lock.lock(); // Acquire the lock
        try {
            if (hashMap.containsKey(city)) {
                // Key exists, so retrieve the list and add the Order
                List<Order> orderList = hashMap.get(city);
                orderList.add(order);
                for (Order _order : orderList) {
                    _order.displayOrder();
                }
            } else {
                // Key doesn't exist, so create a new list, add the Order, and put it in the HashMap
                List<Order> orderList = new ArrayList<>();
                hashMap.put(city, orderList);
                ordersQueue.add(order);
            }
        } finally {
            lock.unlock(); // Release the lock in a finally block to ensure it's always released
        }
    }

    public static void deleteFromOrderList(String key, Order order) {
        lock.lock(); // Acquire the lock
        try {
            if (hashMap.containsKey(key)) {
                List<Order> orderList = hashMap.get(key);
                orderList.remove(order);

            }
        } finally {
            lock.unlock(); // Release the lock in a finally block to ensure it's always released
        }
    }



    // Get all orders from the queue
    public static Queue<Order> getOrders() {
        return ordersQueue;
    }

    // Print all orders in the queue
    public static void printAllOrders() {
        lock.lock(); // Acquire the lock
        try {
            System.out.println("List of Orders:");
            for (Order order : ordersQueue) {
                order.displayOrder();
            }
        } finally {
            lock.unlock(); // Release the lock in a finally block to ensure it's always released
        }


}
}
