package classes;

public abstract class Dish {

    private int price;

    // Constructor for Dish
    public Dish( int price) {

        this.price = price;
    }



    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
