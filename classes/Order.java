package classes;

public class Order {

    private String name;
    private String city;
    private Dish dish;

    public Order(String name, String city, Dish dish) {
        this.name = name;
        this.city = city;
        this.dish = dish;
    }

    // Getters for name, city, and dish
    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public Dish getDish() {
        return dish;
    }

    // Display the order details
    public void displayOrder() {
        System.out.println("Customer Name: " + name);
        System.out.println("City: " + city);
        System.out.println("Price Dish: " + dish.getPrice() );
    }

}
