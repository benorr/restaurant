package classes;
enum PastaType {
    SPAGHETTI,
    PENNE,
    TORTELLINI,

}
 enum Sauce {
    TOMATOES,
    MUSHROOM_CREAM,
    ROSA

}



public class Pasta extends Dish {
    private PastaType pastaType;
    private Sauce sauce;
    public Pasta( PastaType pastaType, Sauce sauce) {
        super( 50);
        this.pastaType = pastaType;
        this.sauce = sauce;
    }
    public Sauce getSauce() {
        return sauce;
    }
    public PastaType getPastaType() {
        return pastaType;
    }


    public void setPastaType(PastaType pastaType) {
        this.pastaType = pastaType;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }




}
