package classes;

import java.util.Scanner;

// Define an ENUM for dish types
enum DishType {
    PASTA,
    PIZZA
}
public class DishFactory {
    // Scanner for user input
    static Scanner scanner = new Scanner(System.in);
    public static Dish createDish(DishType type) {
        switch (type) {
            case PASTA:
                String input;
                System.out.print("Enter a Pasta type (SPAGHETTI,PENNE,TORTELLINI): ");
                input = scanner.nextLine();
                // Convert the user input to an Enum value
                PastaType pastaType = null;
                try {
                    pastaType = PastaType.valueOf(input.toUpperCase());
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid dish type.");

                    return null;
                }

                System.out.print("Enter a Pasta type (TOMATOES,MUSHROOM_CREAM,ROSA): ");
                input = scanner.nextLine();
                // Convert the user input to an Enum value
                Sauce sauce = null;
                try {
                    sauce = Sauce.valueOf(input.toUpperCase());
                } catch (IllegalArgumentException e) {
                    System.out.println("Invalid dish type.");

                    return null;
                }

                return new Pasta(pastaType,sauce );
            case PIZZA:

                System.out.print("Do you want Extras?(Yes/No): ");
                input = scanner.nextLine();
                input.toUpperCase();
                if(input.equals("Yes")){
                    System.out.print("Which  Extras do you want? ");
                    input = scanner.nextLine();

                    return new Pizza(input);
                }
                else
                    return new Pizza();

            default:
                throw null;
        }
    }


}
