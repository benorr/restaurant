import classes.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        // Create a Restaurant instance
        Restaurant restaurant = new Restaurant();
        // Create three Order objects and add them to the queue
        Order order1 = new Order("Customer 1", "elad", new Pizza());
        Order order2 = new Order("Customer 2", "tel aviv", new Pizza());
        Order order3 = new Order("Customer 3", "haifa", new Pizza());
        Order order4 = new Order("Customer 4", "elad", new Pizza());

        restaurant.addOrder(order1);
        restaurant.addOrder(order2);
        restaurant.addOrder(order3);
        restaurant.addOrder(order4);
        OrderChecker orderProcessorThread = new OrderChecker();
        orderProcessorThread.start();
        // Wait for the OrderChecker thread to finish
//        try {
//            orderProcessorThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Menu.runMenu(restaurant);


    }
}